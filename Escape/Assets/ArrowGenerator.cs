﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowGenerator : MonoBehaviour
{
    public GameObject RedArrowPrefab;
    float RedSpan = 0.3f;
    float RedDelta = 0;


    public GameObject WhiteArrowPrefab;
    float WhiteSpan = 0.3f;
    float WhiteDelta = 0;

    // Update is called once per frame
    void Update()
    {
        this.RedDelta += Time.deltaTime;
        if (this.RedDelta > this.RedSpan)
        {
            this.RedDelta = 0;
            GameObject go = Instantiate(RedArrowPrefab) as GameObject;
            int px = Random.Range(-6, 6);
            go.transform.position = new Vector3(7,px, 0);
        }
        this.WhiteDelta += Time.deltaTime;
        if (this.WhiteDelta > this.WhiteSpan)
        {
            this.WhiteDelta = 0;
            GameObject go = Instantiate(WhiteArrowPrefab) as GameObject;
            int px = Random.Range(-6, 6);
            go.transform.position = new Vector3(7, px, 0);
        }
    }
}
